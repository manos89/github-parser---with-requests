import argparse
from GithubParser import *


def main():
    base_url = "https://github.com/search?q={0}&type={1}"

    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="give a json file as input input")
    parser.add_argument("--output", help="give a json file as output")
    args = parser.parse_args()

    json_input = read_json_input(args.input)

    results = []

    for keyword in json_input["keywords"]:
        if json_input["type"] == "Repositories":
            for result in Repository(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        elif json_input["type"] == "Issues":
            for result in Issue(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        elif json_input["type"] == "Wikis":
            for result in Wiki(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        else:
            print("Give a valid input type")

    write_results(results, args.output)


if __name__ == "__main__":
    main()
