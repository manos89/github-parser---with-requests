import requests
import re
import json
from bs4 import BeautifulSoup
import os
import random


class GithubParser:

    session = requests.Session()
    session.headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                       "Chrome/72.0.3626.119 Safari/537.36"}
    session.proxies = None
    proxy_counter = 0

    """The constructor of the GithubParser class. We initiate the class by passing the url and the proxies as 
    parameters"""
    def __init__(self, url, proxies):
        self.url = url
        self.proxies = proxies
        self.session.proxies = {"http": "http://" + self.proxies[random.randint(0, len(self.proxies)-1)]}

    """"This is a basic get_response function that tries to get the response from a  url. If this failes then it 
    changes proxy and tries again"""
    def get_response(self):
        try:
            return self.session.get(self.url, timeout=20).text
        except requests.RequestException:
            self.rotate_proxies()

    """This function is used to rotate proxies when the current proxy is banned from the website. I have used a variable
    named proxy_counter so I can keep track each time on which proxy I am using."""
    def rotate_proxies(self):
        self.session.proxies = {"http": "http://" + self.proxies[self.proxy_counter]}
        if self.proxy_counter < len(self.proxies)-1:
            self.proxy_counter += 1
        else:
            self.proxy_counter = 0


"""I have created 3 children classes below, one for each category (repos, issues, wikis). Each page has different 
html structure so and also I had to collect language stats from the repos. All the classes have the parse method 
(slightly different on each one) and the Repository class has a method that returns the language stats. I  have used
beautifulsoup and html.parser as my marker and located the elements  using the HTML DOM. Each parse method yield the
results in the dictionary format I've been asked"""


class Repository(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("li", {"class": "repo-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find("a")["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            item_response = GithubParser.get_response(self)
            yield {"url": self.url, "owner": owner, "language_stats": self.get_language_stats(item_response)}

    def get_language_stats(self, item_html_response):
        soup = BeautifulSoup(item_html_response, "html.parser")
        language_stats = {}
        try:
            language_stats = {lang.find("span", {"class": "lang"}).text:
                              float(lang.find("span", {"class": "percent"}).text.strip("%"))
                              for lang in soup.find_all("ol", {"class": "repository-lang-stats-numbers"})[0]
                                  .find_all("li")}
        except IndexError:
            pass
        return language_stats


class Issue(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("div", {"class": "issue-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find("a")["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            yield {"url": self.url, "owner": owner, "language_stats": {}}


class Wiki(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("div", {"class": "wiki-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find_all("a")[-1]["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            yield {"url": self.url, "owner": owner, "language_stats": {}}


"""Simple function that reads the json input. If it can't find the file then it raises an Exception"""


def read_json_input(input_file):
    try:
        input_file = os.path.join("json_files", input_file)
        text = open(input_file, encoding="utf8")
        input_parameters = json.loads(text.read())
        text.close()
        return input_parameters
    except (OSError, IOError):
        raise Exception('Could not find the file. Please change the filename')


"""Simple  function that creates the folder output (if it doesn't exist) and then writes the results"""


def write_results(dict_results, output_file):
    if not os.path.exists(os.path.join(os.getcwd(), "output")):
        os.makedirs(os.path.join(os.getcwd(), "output"))
    output_file = os.path.join(os.path.join(os.getcwd(), "output"), output_file)
    with open(output_file, 'w') as outfile:
        json.dump(dict_results, outfile, indent=4)


