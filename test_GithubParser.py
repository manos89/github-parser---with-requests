from GithubParser import *
import pytest


class GithubParser:
    session = requests.Session()
    session.headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                                     "Chrome/72.0.3626.119 Safari/537.36"}
    session.proxies = None
    proxy_counter = 0

    def __init__(self, url, proxies):
        self.url = url
        self.proxies = proxies
        self.session.proxies = {"http": "http://" + self.proxies[self.proxy_counter]}

    def get_response(self):
        self.rotate_proxies()
        try:
            return self.session.get(self.url, timeout=20).text
        except requests.RequestException:
            self.rotate_proxies()

    def rotate_proxies(self):
        self.session.proxies = {"http": "http://" + self.proxies[self.proxy_counter]}
        if self.proxy_counter < len(self.proxies) - 1:
            self.proxy_counter += 1
        else:
            self.proxy_counter = 0


class Repository(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("li", {"class": "repo-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find("a")["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            item_response = GithubParser.get_response(self)
            yield {"url": self.url, "owner": owner, "language_stats": self.get_language_stats(item_response)}

    def get_language_stats(self, item_html_response):
        soup = BeautifulSoup(item_html_response, "html.parser")
        language_stats = {}
        try:
            language_stats = {lang.find("span", {"class": "lang"}).text:
                                  float(lang.find("span", {"class": "percent"}).text.strip("%"))
                              for lang in
                              soup.find_all("ol", {"class": "repository-lang-stats-numbers"})[0].find_all("li")}
        except IndexError:
            pass
        return language_stats


class Issue(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("div", {"class": "issue-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find("a")["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            yield {"url": self.url, "owner": owner, "language_stats": {}}


class Wiki(GithubParser):

    def parse(self):
        soup = BeautifulSoup(GithubParser.get_response(self), "html.parser")
        items = soup.find_all("div", {"class": "wiki-list-item"})
        for item in items:
            self.url = "https://github.com" + item.find_all("a")[-1]["href"]
            owner = re.findall("github.com/(.*?)/", self.url)[0]
            yield {"url": self.url, "owner": owner, "language_stats": {}}


def read_json_input(input_file):
    try:
        input_file = os.path.join("json_files", input_file)
        text = open(input_file)
        input_parameters = json.loads(text.read())
        text.close()
        return input_parameters
    except (OSError, IOError):
        raise Exception('Could not find the file. Please change the filename')


def write_results(dict_results, output_file):
    if not os.path.exists(os.path.join(os.getcwd(), "output")):
        os.makedirs(os.path.join(os.getcwd(), "output"))
    output_file = os.path.join(os.path.join(os.getcwd(), "output"), output_file)
    with open(output_file, 'w') as outfile:
        json.dump(dict_results, outfile, indent=4)


@pytest.mark.parametrize(["input_file", "output"], [("input1.json", "output1.json"), ("test1.json", "output1.json"),
                                                    ("test2.json", "output1.json"),
                                                    ("test5.json", "output.json")])
def test_run(input_file, output):
    base_url = "https://github.com/search?q={0}&type={1}"
    json_input = read_json_input(input_file)

    results = []

    for keyword in json_input["keywords"]:
        if json_input["type"] == "Repositories":
            for result in Repository(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        elif json_input["type"] == "Issues":
            for result in Issue(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        elif json_input["type"] == "Wikis":
            for result in Wiki(base_url.format(keyword, json_input["type"]), json_input["proxies"]).parse():
                results.append(result)
        else:
            print("Give a valid input type")

    write_results(results, output)
