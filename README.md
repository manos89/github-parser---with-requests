# Github Parser

Github Parser is a python parser for github (no API).


## **Dependencies**

*  requests 2.21.0
*  beautifulsoup4 4.6.3

## **Input**

When running the script you have to specify an input.
You can either edit the file that it's located inside the json_files directory
or create a new one (in the same directory).
The input should be a JSON file of this format:

> {
>   "keywords": [
>     "keyword1",
>     "keyword2"
>   ],
>   "proxies": [
>     "proxy1",
>     "proxy1"
>   ],
>   "type": "Repositories" || "Issues" || "Wikis"
> }


keywords and proxies are arrays, so you can write as many as you want,
while "type" can only be one out of the 3 options written above.

## **How to run**
python main.py --input {file.json} --output {output.json}

Replace the {file.json} with your input file and the {output.json} with your
output file.

You have to place your input file in the json_files directory.


